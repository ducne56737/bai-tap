-- Question 1: Viết lệnh để lấy ra danh sách nhân viên và thông tin phòng ban của họ

SELECT account_id, email, user_name, full_name, create_date,department_id, department_name
FROM `account` a
JOIN `department` d USING (department_id); 

-- Question 2: Viết lệnh để lấy ra thông tin các account được tạo sau ngày 20/12/2010

SELECT * 
FROM `account` 
WHERE create_date > "2010-12-20";

-- Question 3: Viết lệnh để lấy ra tất cả các developer
SELECT *
FROM `account` 
JOIN `position`  USING (position_id)
WHERE position_name = "Dev";

-- Question 4: Viết lệnh để lấy ra danh sách các phòng ban có >3 nhân viên
SELECT department_id, department_name
FROM `department`
JOIN `account` USING (department_id)
GROUP BY department_id
HAVING COUNT(department_id) >= 3;

-- Question 5: Viết lệnh để lấy ra danh sách câu hỏi được sử dụng trong đề thi nhiều nhất
WITH cte_max AS (SELECT COUNT(question_id) AS sl 
FROM exam_question
GROUP BY question_id)

SELECT eq.question_id, COUNT(question_id)
FROM exam_question eq
GROUP BY eq.question_id
HAVING COUNT(question_id) = (SELECT MAX(sl) FROM cte_max);

-- Question 6: Thông kê mỗi category Question được sử dụng trong bao nhiêu Question
SELECT category_id, COUNT(category_id) AS thong_ke
FROM `question` q
GROUP BY category_id;

-- Question 7: Thông kê mỗi Question được sử dụng trong bao nhiêu Exam
SELECT  question_id, COUNT(question_id) AS sl_exam
FROM exam_question 
GROUP BY question_id;

-- Question 8: Lấy ra Question có nhiều câu trả lời nhất


-- Question 10: Tìm chức vụ có ít người nhất
WITH cte_min AS (SELECT COUNT(position_id) AS itnhat 
FROM `account`
GROUP BY position_id)

SELECT position_id, position_name, COUNT(position_id)
FROM `account` a
JOIN position USING (position_id)
GROUP BY a.position_id
HAVING COUNT(position_id) = (SELECT MIN(itnhat) FROM cte_min);

-- Question 11: Thống kê mỗi phòng ban có bao nhiêu dev, test, scrum master, PM
SELECT department_id, position_name, COUNT(position_name) AS so_luong
FROM `account`
JOIN `position` USING (position_id)
JOIN `department` USING (department_id)
GROUP BY position_name, department_id ;

-- Question 14:Lấy ra group không có account nào
SELECT *
FROM group_account ga
RIGHT JOIN `group` g USING (group_id)
WHERE ga.group_id IS NULL;

-- -- Question 16: Lấy ra question không có answer nào
SELECT q.* 
FROM `question` q
LEFT JOIN `answer` a USING(question_id)
WHERE a.question_id IS NULL;



