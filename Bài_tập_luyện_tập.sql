DROP DATABASE IF EXISTS `fresher`;
CREATE DATABASE `fresher`;
USE `fresher`;

DROP TABLE IF EXISTS `trainee`;
CREATE TABLE `trainee`(
	trainee_id					TINYINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    full_name					VARCHAR(50) NOT NULL,
    birth_date					DATE,
    `gender`					VARCHAR(15),
    ET_IQ						TINYINT UNSIGNED,
	ET_Gmath					TINYINT UNSIGNED,
	ET_English					TINYINT UNSIGNED,
    Training_Class				VARCHAR(30),
	Evaluation_Notes			VARCHAR(30)
);
INSERT INTO `trainee`	( trainee_id, full_name, birth_date, `gender`, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes )
VALUE 				
					(1, 'Nguyen Thich Nhac', 	'1999-12-15', 'MALE', 		20,	 11, 30, 'VTI001', 'DHBKHN'),
					(2, 'Vtiaccademy', 			'2000-11-27', 'MALE', 		17,	 16, 47, 'VTI002', 'DHQGHN'),
					(3, 'Van Chien', 			'2001-10-03', 'MALE', 		19,	 15, 35, 'VTI001', 'HVBCVT'),
					(4,  'Thi Ngoc', 			'2002-09-28', 'FEMALE',  	9,	 18, 20, 'VTI004', 'DHQGHN'),
					(5, 'Huan Hoa Hong',  		'1998-07-12', 'UNKNOWN', 	20,  8, 49,  'VTI003', 'HVBCVT'),
					(7,  'Huan Hoa Hong',	    '1998-07-12', 'UNKNOWN', 	10,  12, 33, 'VTI001', 'HVBCVT'),
					(8, 'Doc Co Cau Bai',       '1992-06-04', 'FEMALE', 	19,  19, 22, 'VTI003', 'DHQGHN'),
					(6, 'Nguyen Ngoc Nga', 		'1996-05-03', 'MALE', 		18,	 19, 15, 'VTI003', 'DHBKHN'),
					(9, 'Duong Huu', 			'1991-05-02', 'MALE', 		16,	 10, 38, 'VTI002', 'HVBCVT'),
					(10, 'Vtiaccademyc', 		'1999-01-01', 'FEMALE',		8,	 20, 10, 'VTI001', 'DHBKHN');

-- Question 3: Insert 1 bản ghi mà có điểm ET_IQ =30. Sau đó xem kết quả.
INSERT INTO `trainee`	( trainee_id, full_name, birth_date, `gender`, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes )
VALUE 	(11, 'question3', '1990-12-01', 'MALE', 	30,	 20, 50, 'VTI001', 'DHQGHN'
);
SELECT * FROM  `trainee`;

-- Question 4: Viết lệnh để lấy ra tất cả các thực tập sinh đã vượt qua bài test đầu vào,
-- và sắp xếp theo ngày sinh. Điểm ET_IQ >=12, ET_Gmath>=12, ET_English>=20
SELECT * 
FROM `trainee`
WHERE ET_IQ >=12 AND ET_Gmath>=12 AND ET_English>=20
ORDER BY  birth_date DESC;

-- Question 5: Viết lệnh để lấy ra thông tin thực tập sinh có tên bắt đầu bằng chữ N 
-- và kết thúc bằng chữ C
WITH CTE AS (SELECT t.*,substring_index(full_name, ' ', -1) AS `ki_tu_dau_la"N"`
FROM `trainee` AS t) 

SELECT * 
FROM CTE
WHERE `ki_tu_dau_la"N"` LIKE 'N%c';

-- Question 6: Viết lệnh để lấy ra thông tin thực tập sinh
--  mà tên có ký thự thứ 2 là chữ G
WITH CTE AS (SELECT substring_index(full_name, ' ', -1) AS `ki_tu_th2_la"g"`
FROM `trainee`)

SELECT *
FROM CTE
WHERE `ki_tu_th2_la"g"` LIKE '_G%';

-- Question 7: Viết lệnh để lấy ra thông tin thực tập sinh 
-- mà tên có 10 ký tự và ký tự cuối cùng là C
WITH CTE AS (SELECT t.*,substring_index(full_name, ' ', -1) AS `ki_tu_cuoi_la"c"`
FROM `trainee` AS t)            

SELECT *
FROM CTE 
WHERE LENGTH(`ki_tu_cuoi_la"c"`)>=10 AND `ki_tu_cuoi_la"c"` LIKE '%c' ;

-- Question 8: Viết lệnh để lấy ra Fullname của các thực tập sinh trong lớp, 
-- lọc bỏ các tên trùng nhau.
SELECT DISTINCT full_name
FROM  `trainee`;

-- Question 9: Viết lệnh để lấy ra Fullname của các thực tập sinh trong lớp, 
-- sắp xếp các tên này theo thứ tự từ A-Z.
SELECT full_name
FROM  `trainee`
ORDER BY full_name;
 
-- Question 10: Viết lệnh để lấy ra thông tin thực tập sinh có tên dài nhất
WITH CTE_max AS (SELECT LENGTH(full_name) dai_nhat
FROM `trainee` )

SELECT *
FROM  `trainee`
WHERE LENGTH(full_name) = 
( SELECT MAX(dai_nhat) FROM CTE_max);

-- Question 11: Viết lệnh để lấy ra ID, Fullname 
-- và Ngày sinh thực tập sinh có tên dài nhất
WITH CTE_max AS (SELECT LENGTH(full_name) dai_nhat
FROM `trainee` )

SELECT trainee_id, full_name, birth_date
FROM  `trainee`
WHERE LENGTH(full_name) = 
( SELECT MAX(dai_nhat) FROM CTE_max);

-- Question 12: Viết lệnh để lấy ra Tên, và điểm IQ, Gmath, 
-- English thực tập sinh có tên dài nhất
WITH CTE_max AS (SELECT LENGTH(full_name) dai_nhat
FROM `trainee` )

SELECT full_name, ET_IQ, ET_Gmath, ET_English
FROM  `trainee`
WHERE LENGTH(full_name) = 
( SELECT MAX(dai_nhat) FROM CTE_max);

-- Question 13 Lấy ra 5 thực tập sinh có tuổi nhỏ nhất
SELECT trainee_id, full_name, birth_date
FROM `trainee`
ORDER BY birth_date DESC LIMIT 5;

-- Question 14: Viết lệnh để lấy ra tất cả các thực tập sinh là ET, 
-- 1 ET thực tập sinh là những người thỏa mãn số điểm như sau:
-- ∙ ET_IQ + ET_Gmath>=30
-- ∙ ET_IQ>=8
-- ∙ ET_Gmath>=8
-- ∙ ET_English>=18
SELECT *
FROM `trainee`
WHERE   (ET_IQ + ET_Gmath) >=30 
		AND ET_IQ>=8 AND ET_Gmath>=8 
		AND ET_English>=18;

-- Question 17: Xóa thực tập sinh quá 30 tuổi.
-- SELECT birth_date 
-- FROM `trainee` 
-- WHERE  2022 - birth_date >= 30 ;

-- Question 20: Đếm xem trong lớp VTI001  có bao nhiêu thực tập sinh.
SELECT Training_Class, COUNT(Training_Class) soluong_hs
FROM `trainee`
GROUP BY Training_Class;

-- Question 22: Đếm tổng số thực tập sinh trong lớp VTI001 và VTI003 có bao nhiêu thực tập sinh.







