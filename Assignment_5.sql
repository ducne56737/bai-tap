-- Question 1: Tạo view có chứa danh sách nhân viên thuộc phòng ban sale
CREATE OR REPLACE VIEW view_list_sale AS SELECT *
FROM `account` 
JOIN `department`  USING (department_id)
WHERE department_name = 'Sale';

SELECT * FROM view_list_sale;

-- Question 2: Tạo view có chứa thông tin các account tham gia vào nhiều group nhất

-- Question 4: Tạo view có chứa danh sách các phòng ban có nhiều nhân viên nhất
CREATE OR REPLACE VIEW view_max_nv AS
SELECT department_id, department_name, COUNT(department_id) AS sl_nv
FROM `account` a
JOIN `department` d USING (department_id)
GROUP BY department_id
HAVING COUNT(department_id) = (SELECT MAX(dem) max_nv
FROM (SELECT COUNT(department_id) dem FROM `account` GROUP BY department_id) nhom_lai);

 SELECT * FROM view_max_nv;


