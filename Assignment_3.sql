-- Question 3: lấy ra id của phòng ban "Sale"
SELECT department_id
FROM `department`
WHERE department_name = "Sale";

-- Question 4: lấy ra thông tin account có full name dài nhất
SELECT * 
FROM `account` 
WHERE length(full_name) = (SELECT MAX(length(full_name)) FROM `account`);

-- Question 5: Lấy ra thông tin account có full name dài nhất
-- và thuộc phòng ban có id = 3
SELECT * 
FROM `account` 
WHERE length(full_name) = 
(SELECT MAX(length(full_name)) FROM `account`) AND department_id = 3;
-- CACH 2
SELECT *
FROM (SELECT * FROM `account` WHERE department_id = 3) AS department_table
WHERE length(full_name) = (SELECT MAX(length(full_name))
FROM (SELECT * FROM `account` WHERE department_id = 3) AS department_table);

-- Question 6: Lấy ra tên group đã tham gia trước ngày 22019-03-06
SELECT group_name
FROM `group` 
WHERE create_date < "2019-03-06";

-- Question 7: Lấy ra ID của question có >= 4 câu trả lời
SELECT question_id
FROM answer 
GROUP BY question_id
HAVING COUNT(question_id)>=4;

-- Question 8: Lấy ra các mã đề thi có thời gian thi >= 60 phút 
-- và được tạo trước ngày 20/12/2019
SELECT exam_id, duration, create_date
FROM `exam`
WHERE duration >= 60 AND create_date < "2019-12-20";

-- Question 9: Lấy ra 5 group được tạo gần đây nhất
SELECT * 
FROM `group`
ORDER BY create_date LIMIT 5; 

-- Question 10: Đếm số nhân viên thuộc department có id = 3
SELECT department_id, COUNT(account_id) AS sl_nv
FROM `account`
GROUP BY department_id
HAVING department_id = 3;

-- Question 11: Lấy ra nhân viên có tên bắt đầu bằng chữ "D" và kết thúc bằng chữ "o"
SELECT full_name
FROM `account`
WHERE full_name LIKE "D%o";





